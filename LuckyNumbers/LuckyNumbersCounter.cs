namespace LuckyNumbers;

public class LuckyNumbersCounter
{
    private readonly int _totalDigits;
    private readonly int _digits;
    private readonly int _calculationSystem;
    private readonly bool _isDebugLevel;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="totalDigits">Общее количество цифр</param>
    /// <param name="digits">Количество сравниваемых цифр</param>
    /// <param name="calculationSystem">Система исчисления</param>
    /// <param name="isDebugLevel">Режим отладки или нет, для простоты использую параметр!</param>
    public LuckyNumbersCounter(int totalDigits, int digits, int calculationSystem, bool isDebugLevel = false)
    {
        // Тут разные проверки на корректность значений, например
        if (totalDigits < digits * 2)
        {
            throw new ArgumentException("Общее количество цифр долждно быть больше или равно количеству сравниваемых цифр");
        }
        
        _totalDigits = totalDigits;
        _digits = digits;
        _calculationSystem = calculationSystem;
        _isDebugLevel = isDebugLevel;
    }
    
    /// <summary>
    /// Выполнить подсчет красивых номеров
    /// </summary>
    /// <returns>Число красивых номеров</returns>
    public long GetLuckyNumbersCount()
    {
        var maxValue = _calculationSystem - 1; 
        var maxSum = maxValue * _digits;

        Log($"maxSum={maxSum}");

        var arrayOfSum = new long[maxSum + 1]; // с учетом нулевого элемента +1

        FillArrayOfSum(arrayOfSum, 0,_digits, maxValue);
        
        return SumOfSquares(arrayOfSum);
    }

    /// <summary>
    /// Подсчитать суммы для всех возможных наборов разрядов и заполнить их в массив
    /// </summary>
    private void FillArrayOfSum(long[] arrayOfSum, int sum, int digitDepth, int maxValue)
    {
        for (var d = 0; d <= maxValue; d++)
        {
            if (digitDepth > 1)
            {
                FillArrayOfSum(arrayOfSum, sum + d, digitDepth - 1, maxValue);
            }
            else
            {
                arrayOfSum[sum + d]++;
            }
        }
    }

    /// <summary>
    /// Количество красивых номеров равно сумме каждого значения массива в квадрате 
    /// </summary>
    private long SumOfSquares(long[] arrayOfSum)
    {
        var middleDigitsRation =
            _totalDigits - _digits * 2 <= 0 ? 1 : (_totalDigits - _digits * 2) * _calculationSystem;
        
        long result = 0;
        for (var i = 0; i < arrayOfSum.Length; i++)
        {
            var item = arrayOfSum[i];
            result += item * item * middleDigitsRation;
            
            Log($"item[{i}]={item} - result={result}");
        }

        return result;
    }
    
   private void Log(string message)
    {
        if (!_isDebugLevel)
            return;
    
        Console.WriteLine(message);
    }
}