# LuckyNumbersCounter
Подсчитывает красивые номера и выводит на экран
https://docs.google.com/document/d/10pQYlNvjFpJzoSd95Mld9NTxX1JXNAGR/edit

Правильный ответ: 9 203 637 295 151

## 1. class LuckyNumbersCounter
Инкапсулирует логику
- Конструктор принимает общее количество цифр, количество разрядов, которые нужно сравнивать и систему счисления

Например, new LuckyNumbersCounter(13, 6, 13);
13 - общее количество цифр в номере
6 - количество сравниваемых цифр (первых и последних)
13 - система исчисления

## 2. Main
- осуществляет вызов метода подсчета класса
- печатает в стандартный вывод количество 13-ти значных красивых чисел с ведущими нулями в тринадцатиричной системе исчисления

## Links

- https://habr.com/ru/post/266479/ - Счастливые билетики до 300 цифр
- https://oyla.xyz/article/matematika-udaci-isem-scastlivye-bilety
- https://rsdn.org/forum/cpp/8132613.hot - тут я понял, что не учитываю 13 цифру и что нужно домножить для каждого набора счастливого числа на 13
- https://youtu.be/tWho1j2zHG4